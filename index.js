"use strict";
import { CircleC1, NewClass } from "./oopie.js";

let n = 10;
// Primitives by Value // Object by reference
let nO = { nvalue: 10 };
function increament(inputValue, inputObj) {
  inputValue++;
  inputObj.nvalue++;
};
console.log(n, nO.nvalue);
increament(n, nO);
console.log(n, nO.nvalue);

const newC = new NewClass(3);
const Cc1 = new CircleC1(3);

newC.draw();
newC.redraw();

NewClass.halfC = { area: "half of circle" };
NewClass.funcHalfC = function () {
  console.log("Half Circle area and perimeter function");
};


console.log(newC);
console.log(Cc1);
// console.log(NewClass.halfC);
// console.log(NewClass.funcHalfC);
// for (let key in newC) {
//   console.log(typeof newC[key]);
//   console.log(key);
// };

/*
const keyList = Object.keys(newC);
for (let key of keyList) {
  console.log(key);
  console.log(newC[key]);
};
if ('radius' in newC) {
  console.log(newC.radius);
  console.log(newC);
};
 */

//currying basic example
function add(a) {
  return function (b) {
    return function (c) {
      console.log('A', a, 'B', b, 'C', c);
      return a + b + c;
    };
  };
}
console.log(add(1)(2)(3));
console.log(add);
console.log(add(12));
console.log(add(12)(13));
console.log(add(12)(13)(14));
