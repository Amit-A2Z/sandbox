"use strict";

const _redii = new WeakMap();

function oopie() {
    console.log("Hello World from OOPIE ⚛️💥");
};

//Factory Function
function ffCreateCircle(radius) {
    return {
        radius,
        draw() {
            console.log("Factory produce a circle 🏭");
        }
    };
};

//Constructor Function
function Circle(radius) {
    console.log("This C1 is an: ", this);
    _redii.set(this, radius * 3);
    this.radius = radius;
    this.draw = function () {
        console.log("Construct a circle 🏗️🏗️");
        console.log(_redii.get(this));
    };
    this.pie = function () {
        console.log("PIE ");
    };

};

class CircleC1 {
    constructor(radius) {
        console.log("This C1 is an: \n", this);
        this.radius = radius;
        _redii.set(this, radius * 3);
        console.log(`weakMap Radius is ⭕🛞🎯: ${_redii.get(this)}`);
        this._ciIntPair = Symbol("num of x,y pair integers in circle");
        this.coOrdinates = function (x = 0, y = 0) {
            console.log(`Default Center 🎯 Co-ordinates: ${x}, ${y}`);
        };
    };

    static cs1Parse() {
        console.log("CS1 Parse 🌲");
    }

    area() {
        return Math.PI * Math.pow(this.radius, 2);
    }
    perimeter() {
        return 2 * Math.PI * this.radius;
    }
    draw() {
        console.log("CircleC1 draw method called 🏗️⚠️");
    }

};

const _draw = Symbol("draw");
class NewClass extends CircleC1 {

    constructor(radius) {
        super(radius);
        // this.key1 = "value1";
        // this.key2 = "value2";
        this.attr1 = "Attribute Value 1";
        this.attr2 = "Attribute Value 2";
        console.log("This NewClass Constructor");
        console.log(`Is [] == [] : ${[] == []}`);
        console.log(`Is Symbol() == Symbol() : ${Symbol() == Symbol()}`);
        this.polarCoordinates = function (radius = 0, angle = 0) {
            console.log(`Polar Co-ordinates 🎯: ${radius}, ${angle}`);
        };
    };

    [_draw](radius) {
        // TODO document why this method is empty

    };

    static _ciInt = Symbol("num of integers in circle");


    static ncs1Parse() {
        console.log("NewClass Static 1 Parse 🌲");
    }

    draw() {
        CircleC1.prototype.draw.call(this, this.radius);
        console.log("Construction of a class-circle 🛞🛞🛞");

    };
    redraw() { console.log("New redraw ⭕"); };
};

export {
    oopie,
    ffCreateCircle,
    Circle,
    CircleC1,
    NewClass
};
