## OOPS in JavaScript

- Prefer Constructor function over Factory function
  - Better error handling
  - Better performance

## Stackoverflow Tests

- [Stackoverflow Test](stack.js)
  Error could be due to: Frames crossing the stack limit of ~10K-16K
  Stack Memory limit of ~900KB exceeded within one/multiple frame at the runtime
  Not recommended to manually change the stack memory limit as it is not documented and unstable and may not work as expected

## Summary

## Changelog
