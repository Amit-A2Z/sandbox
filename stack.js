


//recursion stackoverflow test 
let rrrCount = 0;
let rrrError = false;
let rrrmanualStop = 500000;
function rrr(value) {
    rrrCount++;
    if (value % 10000 === 0) {
        console.log('rrrCounter value: ' + value);
        let memoryUsage = process.memoryUsage();
        for (let key in memoryUsage) {
            memoryUsage[key] = (memoryUsage[key] / 1024 / 1024).toFixed(3) + ' MB';
            console.log(key + ': ' + memoryUsage[key]);
        };
    };
    if (value >= rrrmanualStop) {
        console.log(`Reached the manual stop limit of recursion: ${rrrmanualStop} times`);
        return value;
    };
    return rrr(value + 1);
};

try {
    rrr(0);
} catch (error) {
    //don't rely much on catching the stack overflow error. It could fail without catching it!!! 
    console.log(" 🚩 Stack overflow recursion error depth: " + rrrCount);
    console.error(error);
};

